# Veille

## [Présentation](https://gitpitch.com/PEGeoffroy/veille/master?grs=gitlab)

### [Dépôt](https://gitlab.com/PEGeoffroy/veille/)

+++

## Sommaire

@ol

- RSS
- Shaarli
- Usage

@olend

---

## Attribution

@ul

- Certaines informations proviennent de Wikipédia, Framasoft.

@ulend

---

## RSS

![feed_icon](images/feed_icon.png)

+++

### Définition

Le RSS est un format de fichier qui contient la liste des nouveautés ou des informations d'un site.

Note:
- **Atom** est autre exemple de format de fichier.
- Pour la petite histoire, **Aaron Swartz** à participé à son élaboration.

+++

### Lecteur de flux RSS

@ul

- Un lecteur de flux RSS regroupe tout les flux des sites que l'on a sauvegardé. @note[C'est ce qu'on appelle un agrégateur.]
- Ça permet d’être toujours au courant de l’actualité des sites susceptible de fournir une veille intéressante. @note[Notez bien que tous les sites ne fournissent pas forcément un flux RSS et s’il le font, il est possible que les articles ne soient pas complets.]
- [Exemple](http://guepe.ateliez.fr/rss/p/i/)

@ulend

Note:
- Il est très facile d'installer ces apps sur serveur, même sur les mutualisés.

+++

### Screenshot

![rss](images/rss.png)

Note:
- Vous trouverez la plupart du temps sur les sites une petite icône. Il s’agit d’un lien vers le flux RSS du site.

+++

### Logiciel

@ul

- [FreshRSS](https://www.freshrss.org/)
- [QuiteRSS](https://quiterss.org/en/download)
- [...](http://guepe.ateliez.fr/shaarli/?searchterm=rss&searchtags=)

@ulend

Note:
- Il existe un plugin dans tout les navigateurs pour récupérer directement le flux.

+++

## Liens

@ul

- [RSS (!w.fr)](https://fr.wikipedia.org/wiki/RSS)
- [Aaron Swartz (!w.fr)](https://fr.wikipedia.org/wiki/Aaron_Swartz)

@ulend

---

## Shaarli

+++

### Qu'est-ce que c'est ?

Shaarli pour *shaare link* est une app web qui permet :

@ol

- La sauvegarde des liens
- L'ajout de description
- L'ajout de tags

@olend

+++

### Présentation

![shaarli_reception](images/shaarli_reception.png)

Note:
- Ici vous voyez la page d'accueil de shaarli
- Barre de recherche
- Un shaare (titre, description, tag,)

+++

### Le shaare

![shaarli_update](images/shaarli_update.png)

Note:
- Rédaction Md
- public / privé

+++

### Les tags

![shaarli_tags](images/shaarli_tags.png)

---

## Usage

@ul

- La combinaison RSS/Shaarli. @note[On peut rajouter 2 raccourcis pour fluidiser notre veille]
- Une veille : comme un journal ! @note[On peut catégoriser nos flux]

@ulend

---

## Intérêt

@ul

- Organisation
- Choix/Sélection
- Centralisation de la veille
- Procrastinophobe

@ulend

---

@quote[Donne moi ton shaarli, je te dirais qui tu es.](Moi-même)

---

## lien de la présentation

![qr_code](images/qr_code.png)